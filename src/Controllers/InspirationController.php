<?php
namespace Fhsinchy\Inspire\Controllers;

use Illuminate\Http\Request;
use Fhsinchy\Inspire\Inspire;

class InspirationController
{
    public function __invoke(Request $request): string
    {
        $quote = "Byambasuren";

        return view('inspire::index', compact('quote'));
    }
}
